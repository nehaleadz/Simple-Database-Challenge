import sys

varValHistory = {}
varsModified = []
curTransaction = -1

def PrintVal(val):
	if val is None: 
		print "NULL"
	else:
		print val 

def SetVariable(variable, value):
	if curTransaction > -1:
		list = varsModified[curTransaction]
		list.append(variable)

	if variable in varValHistory:
		valList = varValHistory[variable]
	else:
		valList = []
		valList.append(None)
	valList.append(value)
	varValHistory[variable] = valList

def ProcessInput(line):
	global curTransaction
	parsedInputLine = line.split(" ")
	command = parsedInputLine[0]

	if command == "SET":
		variable = parsedInputLine[1].rstrip()
		value = parsedInputLine[2].rstrip()
		SetVariable(variable, value)
	
	elif command == "GET":
		variable = parsedInputLine[1].rstrip()
		value = None
		if variable in varValHistory:
			 valList = varValHistory[variable]
			 value = valList[-1]
		PrintVal(value)

	elif command == "UNSET": 
		variable = parsedInputLine[1].rstrip()
		SetVariable(variable, None)

	elif command == "NUMEQUALTO":
		value = parsedInputLine[1].rstrip()
		nCount = 0
		for list in varValHistory.values():
			if list[-1] == value:
				nCount = nCount + 1
		print nCount

	elif command == "BEGIN":
		curTransaction = curTransaction + 1
		varsModified.append([])

	elif command == "ROLLBACK":
		if curTransaction < 0 or len(varsModified[curTransaction]) == 0:
			print "NO TRANSACTION"
		else:
			for variable in varsModified[curTransaction]:
				varValHistory[variable].pop()
			varsModified.pop()

		curTransaction = curTransaction - 1

	elif command == "COMMIT":
		if curTransaction < 0:
			print "NO TRANSACTION"
		else:
			for variable in varsModified[curTransaction]:
				size = len(varValHistory[variable])
				del varValHistory[variable][:size-1]
		curTransaction = - 1
		
while (1):
	input = raw_input()
	if (input.rstrip() == "END"):
		break
	ProcessInput(input)
